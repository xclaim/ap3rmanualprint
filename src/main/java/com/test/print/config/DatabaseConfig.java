package com.test.print.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import javax.sql.DataSource;


@Configuration
@PropertySource({"classpath:application.properties"})
@EnableConfigurationProperties({DataSourceProperties.class})
public class DatabaseConfig {
	
//	@Autowired
//	private Environment envi;
	
	@Bean(name = "primaryDataSource")
	@Qualifier("primaryDataSource")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	@ConditionalOnProperty(prefix = "spring.datasource", name = { "url" })
	public DataSource primaryDataSource() {
		
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName(envi.getProperty("spring.datasource.driver-class-name"));
//		dataSource.setUrl(envi.getProperty("spring.datasource.url"));
//		dataSource.setUsername(envi.getProperty("spring.datasource.username"));
//		dataSource.setPassword(envi.getProperty("spring.datasource.password"));
//		return dataSource;
		return DataSourceBuilder.create().build();
	}

}
