package com.test.print.controller;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.print.manager.GetData;
import com.test.print.model.pojo.Location;
import com.test.print.model.request.CheckRequest;
import com.test.print.model.request.ClearRequest;
import com.test.print.model.request.ReportBulkRequest;
import com.test.print.model.request.ReportRequest;
import com.test.print.model.response.BaseResponse;
import org.springframework.http.MediaType;
import com.test.print.service.ReportService;

@RestController()
@RequestMapping("/api")
public class MainController {
	
	@Autowired
	private ReportService reportSvc;
	
	@Autowired
	private GetData getData;
	
	
	protected final Log LOGGER = LogFactory.getLog(getClass());
	
	@PostMapping(value="/report/generate", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse generateReport(@RequestBody ReportRequest req) {
		BaseResponse response = new BaseResponse();
		try {
			String path = req.getPath();
			long start = System.currentTimeMillis();
			LOGGER.info("Generating Report");
			String result = reportSvc.getReport(path, req.getAppid(),null);
			long duration = System.currentTimeMillis() - start;
			LOGGER.info("Process finished in "+duration);
			response.setResponseCode("OK");
			response.setResponseMessage(result);
							
		}catch (Exception e) {
			response.setResponseCode("ERROR");
			response.setResponseMessage(e.getMessage());
		}
		return response;
		}
	
	@PostMapping(value="/report/generatebulk", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse generateBulkReport(@RequestBody ReportBulkRequest req) {
		BaseResponse response = new BaseResponse();
		try {
			String path = req.getPath();
			long start = System.currentTimeMillis();
			LOGGER.info("Generating Report");
			String result = reportSvc.getReport(path, null, req.getAppids());
			long duration = System.currentTimeMillis() - start;
			LOGGER.info("Process finished in "+duration);
			response.setResponseCode("OK");
			response.setResponseMessage(result);
		}catch(Exception e) {
			response.setResponseCode("ERROR");
			response.setResponseMessage(e.getMessage());
		}
		return response;
	}
	
	@PostMapping(value="/report/clear", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse clearFile(@RequestBody ClearRequest req) {
		BaseResponse response = new BaseResponse();
		try {
			String result = reportSvc.clearFile(req.getPath());
			response.setResponseCode("OK");
			response.setResponseMessage(result);
		}catch(Exception e) {
			response.setResponseCode("ERROR");
			response.setResponseMessage(e.getMessage());
		}
		return response;
	}
	
	
	@PostMapping(value = "/report/test",  produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse checkDb(@RequestBody CheckRequest req) {
		BaseResponse response = new BaseResponse();
		
		try {
			List<Location> test = getData.getLocation("1");
			if(test == null) {
				response.setResponseCode("ERROR");
				response.setResponseMessage("cannot connect to db");
			}else if(test.isEmpty()) {
				response.setResponseCode("ERROR");
				response.setResponseMessage("Table is empty or cannot connect to db");
			}else {
				response.setResponseCode("OK");
				response.setResponseMessage("Connected to db");
			}
		}catch(Exception e) {
			response.setResponseCode("ERROR");
			response.setResponseMessage(e.getMessage());
		}
		return response;
		
	}

}
