package com.test.print.manager;

import java.util.List;

import com.test.print.model.pojo.Ap3rData;
import com.test.print.model.pojo.Location;



public interface GetData {
	List<Ap3rData> findLoanByAppid(String appid, String status);
	List<Ap3rData> findLoanByAppids(String[] appids, String status);
	List<Location> getLocation(String id);

}
