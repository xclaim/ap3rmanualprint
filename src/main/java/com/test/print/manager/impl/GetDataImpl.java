package com.test.print.manager.impl;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.print.manager.GetData;
import com.test.print.model.pojo.Ap3rData;
import com.test.print.model.pojo.Location;

@Service("GetData")
public class GetDataImpl implements GetData {
	
	@Autowired
	private EntityManager em;
	
	private String ap3rDataPerAppid = "select acc.id as loan_id, acc.sw_id as sw_id, acc.application_id as application_id, " + 
			"acc.disbursement_date as disbursement_date, " + 
			"loc.code as mms_location_code, loc.name as mms_name, loc.parent_location as parent_location " + 
			"from t_loan_account acc " + 
			"inner join t_customer cust on acc.customer_id = cust.id " + 
			"inner join t_sentra_group sng on cust.group_id = sng.id " + 
			"inner join t_sentra snt on sng.sentra_id = snt.id " + 
			"inner join m_users usr on snt.assign_to = usr.user_login " + 
			"inner join m_location loc on usr.location_id = loc.id " +  
			"where acc.status=:status and acc.application_id=:appid order by loc.name";
	
	private String ap3rDataPerAppIds = "select acc.id as loan_id, acc.sw_id as sw_id, acc.application_id as application_id, " + 
			"acc.disbursement_date as disbursement_date, " + 
			"loc.code as mms_location_code, loc.name as mms_name, loc.parent_location as parent_location " + 
			"from t_loan_account acc " + 
			"inner join t_customer cust on acc.customer_id = cust.id " + 
			"inner join t_sentra_group sng on cust.group_id = sng.id " + 
			"inner join t_sentra snt on sng.sentra_id = snt.id " + 
			"inner join m_users usr on snt.assign_to = usr.user_login " + 
			"inner join m_location loc on usr.location_id = loc.id " +  
			"where acc.status=:status and acc.application_id in :appids order by loc.name";
	
	private String locQuery = "select id,kecamatan,kelurahan,kabupaten,name,address,code,[level] ,parent_location from m_location ml where ml.id =:id ";

	@SuppressWarnings("unchecked")
	@Override
	public List<Ap3rData> findLoanByAppid(String appid, String status) {
		Query q = this.em.createNativeQuery(ap3rDataPerAppid,"Ap3rData");
		q.setParameter("status", status);
		q.setParameter("appid", appid);
		List<Ap3rData> result = q.getResultList();
		em.close();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getLocation(String id) {
		Query q = this.em.createNativeQuery(locQuery,"Location");
		q.setParameter("id", id);
		List<Location> result = q.getResultList();
		em.close();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Ap3rData> findLoanByAppids(String[] appids, String status) {
		Query q = this.em.createNativeQuery(ap3rDataPerAppIds,"Ap3rData");
		List<String> appid = Arrays.asList(appids);
		q.setParameter("status", status);
		q.setParameter("appids", appid);
		List<Ap3rData> result = q.getResultList();
		em.close();
		return result;
	}

}
