package com.test.print.model.pojo;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "Ap3rData", entities = {
		@EntityResult(entityClass = Ap3rData.class, fields = {
				@FieldResult(name = "loanId",column = "loan_id"),
				@FieldResult(name = "swId",column = "sw_id"),
				@FieldResult(name = "appId",column = "application_id"),
				@FieldResult(name = "disbursementDate",column = "disbursement_date"),
				@FieldResult(name = "mmsLocationCode",column = "mms_location_code"),
				@FieldResult(name = "mmsName",column = "mms_name"),
				@FieldResult(name = "mmsParentCode",column = "parent_location"),
		}) })

@Entity
public class Ap3rData {
	
	@Id
	private Long loanId;
	private Long swId;
	private String appId;
	private Date disbursementDate;
	private String mmsLocationCode;
	private String mmsName;
	private String mmsParentCode;
	
	public Long getLoanId() {
		return loanId;
	}
	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}
	public Long getSwId() {
		return swId;
	}
	public void setSwId(Long swId) {
		this.swId = swId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public Date getDisbursementDate() {
		return disbursementDate;
	}
	public void setDisbursementDate(Date disbursementDate) {
		this.disbursementDate = disbursementDate;
	}
	public String getMmsLocationCode() {
		return mmsLocationCode;
	}
	public void setMmsLocationCode(String mmsLocationCode) {
		this.mmsLocationCode = mmsLocationCode;
	}
	public String getMmsName() {
		return mmsName;
	}
	public void setMmsName(String mmsName) {
		this.mmsName = mmsName;
	}
	public String getMmsParentCode() {
		return mmsParentCode;
	}
	public void setMmsParentCode(String mmsParentCode) {
		this.mmsParentCode = mmsParentCode;
	}

	
	

}

