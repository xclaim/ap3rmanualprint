package com.test.print.model.pojo;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "Location", entities = {
		@EntityResult(entityClass = Location.class, fields = {
				@FieldResult(name = "locationId",column = "id"),
				@FieldResult(name = "kabupaten",column = "kabupaten"),
				@FieldResult(name = "kecamatan",column = "kecamatan"),
				@FieldResult(name = "kelurahan",column = "kelurahan"),
				@FieldResult(name = "name",column = "name"),
				@FieldResult(name = "address",column = "address"),
				@FieldResult(name = "locationCode",column = "code"),
				@FieldResult(name = "level",column = "level"),
				@FieldResult(name = "parentLocation",column = "parent_location"),

		}) })

@Entity
public class Location {
	
	@Id
	private String locationId;
	private String kabupaten;
	private String kecamatan;
	private String kelurahan;
	private String name;
	private String address;
	private String locationCode;
	private String level;
	private String parentLocation;
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getKabupaten() {
		return kabupaten;
	}
	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}
	public String getKecamatan() {
		return kecamatan;
	}
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	public String getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getParentLocation() {
		return parentLocation;
	}
	public void setParentLocation(String parentLocation) {
		this.parentLocation = parentLocation;
	}
	
	

}
