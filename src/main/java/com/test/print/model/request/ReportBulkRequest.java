package com.test.print.model.request;

public class ReportBulkRequest {
	private String[] appids;
	private String path;
	public String[] getAppids() {
		return appids;
	}
	public void setAppids(String[] appids) {
		this.appids = appids;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	

}
