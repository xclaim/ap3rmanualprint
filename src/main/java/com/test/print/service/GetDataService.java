package com.test.print.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.print.manager.GetData;
import com.test.print.model.pojo.Ap3rData;
import com.test.print.model.pojo.Location;



@Service("GetDataService")
public class GetDataService {
	@Autowired
	private GetData getDataManager;
	
	public List<Ap3rData> findLoanByAppid(String appid){
		return getDataManager.findLoanByAppid(appid, "APPROVED");
	}
	
	public List<Location> getLocation(String id) {
		return getDataManager.getLocation(id);
	}
	
	public List<Ap3rData> findLoanByAppids(String[] appids) {
		return getDataManager.findLoanByAppids(appids, "APPROVED");
	}
	

}
