package com.test.print.service;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Service("printService")
public class PrintService {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	@Qualifier("primaryDataSource")
	private DataSource primaryDataSource;
	
	@Autowired
	private ResourceLoader resourceLoader;

	public JasperPrint generateJasperPrint(String name, Map<String, Object> parameters) throws Exception{
		Connection conn = primaryDataSource.getConnection();
		try {
			Resource res = resourceLoader.getResource("classpath:static/report/"+name);
			InputStream inputStream = res.getInputStream();
			File reportFile = File.createTempFile("AP3R2", ".jrxml");
			try {
				FileUtils.copyInputStreamToFile(inputStream, reportFile);
			}catch(Exception e) {
				e.printStackTrace();
			}finally{
				IOUtils.closeQuietly(inputStream);
			}
			JasperDesign design = JRXmlLoader.load(reportFile);
			JasperReport report = JasperCompileManager.compileReport(design);
			return JasperFillManager.fillReport(report, parameters, conn);
		}catch (Exception e){
			log.error(e,e);
			return null;
		}
		finally{
			try {
				if(conn!=null && !conn.isClosed()){
					conn.close();
				}
			}catch (Exception e1){
				// ignore the error
			}
		}
	}
	
	public void generatePdf(String name, Map<String, Object> parameters,
            OutputStream outputStream) throws Exception {
        try {
            JasperPrint jasperPrint = generateJasperPrint(name, parameters);
			/*JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
			//log.info("PASSWORD : "+parameters.get("filePass"));
			exporter.setParameter(JRPdfExporterParameter.USER_PASSWORD, parameters.get("filePass"));
			exporter.setParameter(JRPdfExporterParameter.IS_ENCRYPTED, Boolean.TRUE);
			exporter.exportReport();*/
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
        } catch (JRException e) {
            log.error("fail generate pdf", e);
        }
    }

	
	
	
}
