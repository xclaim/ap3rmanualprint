package com.test.print.service;

import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.print.model.pojo.Ap3rData;
import com.test.print.model.pojo.Location;

import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.core.ZipFile;

import org.apache.commons.logging.Log;

@Service("reportService")
public class ReportService {
	
	protected final Log LOGGER = LogFactory.getLog(getClass());
	private int days;
	
	@Autowired
	private GetDataService getDateSvc;
	
	@Autowired
	private PrintService printSvc;
	
	
	public String getReport(String path, String appid,String[] appids )
	throws Exception {
		int ok = 0;
		int fail = 0;
		LOGGER.info("getReport : "+path);
		String fileName = path+"/AP3R_M-Prospera_No_APPID";
		String zipName = path+"/";
		String logFileNameErr = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".error.log";
		String logFileNameOk = zipName + new SimpleDateFormat("yyyyMMdd").format(new Date())+".success.log";
		try {
	        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			Map<String, Object> param = new HashMap<String,Object>();
			List<Ap3rData> data = null;
			if(appids != null) {
				if(appids.length > 0) {
					data = getDateSvc.findLoanByAppids(appids);
				}else {
					data = getDateSvc.findLoanByAppid(appid);
				}
			}else {
				data = getDateSvc.findLoanByAppid(appid);
			}
			int dataCount = 0;
			for(Ap3rData d : data) {
				if(d.getSwId() != null) {
					String appidInfo = "";
					Long start = System.currentTimeMillis();
					Long loanId = d.getLoanId();
					param.put("loanId", loanId);
					String kfoId = d.getMmsParentCode();
					List<Location> kfoLoc = getDateSvc.getLocation(kfoId);
					String kfoCode = kfoLoc.get(0).getLocationCode();
					String kfoName = kfoLoc.get(0).getName();
        			appidInfo = kfoCode + "-" + kfoName + "-" + d.getAppId();

        			File file = new File(fileName + "_" + d.getAppId() + "_" + sdf.format(d.getDisbursementDate()) + ".pdf");
        			ByteArrayOutputStream baos = new ByteArrayOutputStream();
        			printSvc.generatePdf("AP3R.jrxml", param, baos);
        			byte[] bniBytes = baos.toByteArray();
        			FileOutputStream fos = new FileOutputStream(file);
        			fos.write(bniBytes);
        			
        			ZipFile zip = new ZipFile(zipName +d.getMmsLocationCode()+"_"+ d.getMmsName()+".zip");
        			ZipParameters zipParam = new ZipParameters();
        			zipParam.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
        			zipParam.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
        			String dateYMD = String.valueOf(d.getDisbursementDate()).substring(0,10);
        			zipParam.setRootFolderInZip(dateYMD);
        			zip.addFile(file, zipParam);
					fos.flush();
					fos.close();
					file.delete();
					
					File mmsZip = new File(zipName + d.getMmsLocationCode() + "_" + d.getMmsName() + ".zip");
					ZipFile kfoZip = new ZipFile(zipName + kfoLoc.get(0).getLocationCode().trim() + "_" + kfoLoc.get(0).getName().trim() + ".zip");
					ZipParameters zipParam2 = new ZipParameters();
					zipParam2.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
					zipParam2.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
					kfoZip.addFile(mmsZip, zipParam2);
					
					if(dataCount >= data.size()-1) {
						mmsZip.delete();
					}else if(!d.getMmsName().equalsIgnoreCase(data.get(dataCount+1).getMmsName())) {
						mmsZip.delete();
					}
					dataCount++;
					ok++;
					long duration = System.currentTimeMillis()-start;
					saveLogFile(logFileNameOk, "Success generate Report "+appidInfo+" ("+duration+" ms)");
        			
					}else {
					dataCount++;
				}
			}
			
		}catch(Exception e) {
			fail++;
			e.printStackTrace();
			LOGGER.error("error generateReport "+e.getMessage());
			saveLogFile(logFileNameErr,"Error generateReport"+e.getMessage()+"\n"+e.toString());
			return "error generateReport "+e.getMessage();
		}finally{
			LOGGER.info("Generated report ok :"+ok+" fail : "+fail);
		}
		return "Generated report ok :"+ok+" fail : "+fail;
	}
	
	public String clearFile(String path) throws Exception{
		try {
			File files = new File(path);
			File[] oldReport = files.listFiles();
			if(oldReport != null) {
				int i = 0;
				for(File oldFile : oldReport) {
					if(getFileExtension(oldFile).contains("zip")) {
						oldFile.delete();
						i++;
					}
				}
				LOGGER.info(i+" Old file deleted");
				return i+" old file deleted";
			}else {
				LOGGER.info("file already cleared");
				return "file already cleard";
			}
		}catch(Exception e) {
			LOGGER.error("Error deleting old files : "+e.getMessage());	
			return "Error deleting old files : "+e.getMessage();
			}
	}
	
	private String getFileExtension(File file) {
		String filename = file.getName();
		int lastIndexOf = filename.lastIndexOf(".");
		return lastIndexOf <=0 ? "" : filename.substring(lastIndexOf+1);
	}
	
	private void saveLogFile(String filename, String logtxt) {
		try {
			FileWriter fw = new FileWriter(filename,true);
			fw.write(logtxt+"\n");
			fw.close();
		}catch(Exception ex) {
			LOGGER.error("SaveFile Log Exception "+ex.getMessage());
		}
	}
	
	
	public int getDays() {
		return days;
	}
	
	public void setDays(int days) {
		this.days = days;
	}

}
